#include <stdlib.h>
#include <stdio.h>

int aggresiveCowsSolve(int n, int c, int* positions);
int isGapPossible(int N, int C, int* positions, int gap);
int compare (const void * a, const void * b);

int main(void){
	
	int casesNumber = 0;
	scanf("%d", &casesNumber);
	
	int n, c;
	for (int i = 0; i < casesNumber; ++i) {
		scanf("%d", &n);
		scanf("%d", &c);
		int* positions = malloc(n * sizeof(int));
        for (int j = 0; j < n; ++j) {
			scanf("%d", positions + j);
        }
		printf("%d\n", aggresiveCowsSolve(n, c, positions));
		free(positions);
	}
		
	return 0;
}

int aggresiveCowsSolve(int n, int c, int* positions){
	
	qsort(positions,  n, sizeof(int), compare);
	int leftBound = 0;
    int rightBound = positions[n - 1] - positions[0];
    int result = leftBound;
    
    while (leftBound <= rightBound) {
		int middle = (leftBound + rightBound) / 2;
		if (isGapPossible(n, c, positions, middle)) {
			result = middle;
            leftBound = middle + 1;
        } else {
            rightBound = middle - 1;
        }
    }
    
    return result;
}

int isGapPossible(int N, int C, int* positions, int gap) {
            int possibleCows = 1;
            int endPosition = positions[N - 1];
            for (int i = N - 1; i >= 0; --i) {
                if (endPosition - positions[i] >= gap) {
                    possibleCows++;
                    endPosition = positions[i];
                }
            }
            return possibleCows >= C;
}

int compare (const void * a, const void * b)
{
  return (*(int*)a) - (*(int*)b);
}

