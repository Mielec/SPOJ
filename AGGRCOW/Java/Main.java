package pl.mielecmichal.spoj.aggressive.cows;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int casesNumber = 0;
        AggressiveCowsSolver[] solvers = null;

        try (Scanner scanner = new Scanner(System.in)) {
            casesNumber = scanner.nextInt();
            solvers = new AggressiveCowsSolver[casesNumber];
            for (int i = 0; i < casesNumber; ++i) {
                int n = scanner.nextInt();
                int c = scanner.nextInt();
                int[] positions = new int[n];
                for (int j = 0; j < n; ++j) {
                    positions[j] = scanner.nextInt();
                }

                solvers[i] = AggressiveCowsSolver.create(n, c, positions);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        for (int i = 0; i < casesNumber; ++i) {
            System.out.println(solvers[i].solve());
        }

    }


    private static class AggressiveCowsSolver {

        private final int N;
        private final int C;
        private int[] positions;

        private AggressiveCowsSolver(int N, int C, int[] positions) {
            this.N = N;
            this.C = C;
            this.positions = positions;
        }

        public int solve() {
            Arrays.sort(positions);
            int leftBound = 0;
            int rightBound = positions[N - 1] - positions[0];
            int result = leftBound;

            while (leftBound <= rightBound) {
                int middle = (leftBound + rightBound) / 2;
                if (isGapPossible(middle)) {
                    result = middle;
                    leftBound = middle + 1;
                } else {
                    rightBound = middle - 1;
                }
            }

            return result;
        }

        private boolean isGapPossible(int gap) {
            int possibleCows = 1;
            int endPosition = positions[N - 1];
            for (int i = N - 1; i >= 0; --i) {
                if (endPosition - positions[i] >= gap) {
                    possibleCows++;
                    endPosition = positions[i];
                }
            }

            return possibleCows >= C;
        }

        public static AggressiveCowsSolver create(int N, int C, int[] positions) {
            return new AggressiveCowsSolver(N, C, positions);

        }
    }
}