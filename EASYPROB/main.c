//http://www.spoj.com/problems/EASYPROB/

#include <stdio.h>
#include< math.h>
 
#define NUMBERS_COUNT 7
 
void divide_number(uint32_t number); 
uint32_t find_nearby_exponent(uint32_t number);

int main(int argc, char** argv){
 
  uint32_t numbers_to_convertion[NUMBERS_COUNT] = {137, 1315, 73, 136, 255, 1384, 16385};
  for(int i=0; i<NUMBERS_COUNT; i++){
    printf("%d=", numbers_to_convertion[i]);
    divide_number(numbers_to_convertion[i]);
    printf("\n");
  }
  return 0;
}
 
void divide_number(uint32_t number)
{
  if(number == 0){
    return;
  }
 
  uint32_t exponent = find_nearby_exponent(number);
  uint32_t next_number = number - pow(2, exponent);
  
  if(exponent < 3) {
    if(exponent == 1) {
      printf("2"); 
    }
    else {
      printf("2(%d)", exponent); 
    }
   
    if(next_number == 0){
      return;
    }
    printf("+");
    divide_number(next_number);
    return;
  }
 
  printf("2(");
  divide_number(exponent);
  printf(")");
 
  if(next_number == 0){
    return;
  }
  printf("+");
  divide_number(next_number);
}
 
 
uint32_t find_nearby_exponent(uint32_t number) {
  uint32_t mask = 0x80000000;
 
  if(number == 1){
    return 0;
  }
 
  for(uint32_t i=0; i<32; i++) {
    //printf("mask(%x)", mask);
    if((number & mask) > 0){
      return 31-i;
    }
    mask = mask >> 1;
  }
  return -1;
}
